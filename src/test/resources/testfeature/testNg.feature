Feature: Login functionality of DemoWebShop	

Scenario: Verify the login functionality

Given user should be at demowebshop application
Then user be at home page of application
When click on the login option
And user should enter the valid "<EMAIL>" in textbox
And user should enter the valid "<PASSWORD>" in text box
And click on login button
Then user be at home page gets name displayed
And click on logout option
Then user should be at homepage of demowebshop
And The test is Ended close the browser

Examples:
 |EMAIL                  | |PASSWORD    |
 |anusiya1406@gmail.com| |Sivan@1406|
 |manzmehadi1@gmail.com| |Mehek@110|
