package testrunertestng;

import java.time.Duration;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;

import io.cucumber.java.en.Given;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;

public class stepdefTest {
	static WebDriver driver;

	@Given("user should be at demowebshop application")
	public void user_should_be_at_demowebshop_application() {
		driver = new ChromeDriver();
		driver.get("https://demowebshop.tricentis.com/");
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(Duration.ofSeconds(10));
		
	}

	@Then("user be at home page of application")
	public void user_be_at_home_page_of_application() {
		System.out.println("the homepage of the webpage is displayed");

	}

	@When("click on the login option")
	public void click_on_the_login_option() {
		driver.findElement(By.linkText("Log in")).click();

	}

	@When("user should enter the valid {string} in textbox")
	public void user_should_enter_the_valid_in_textbox(String string) {
		driver.findElement(By.id("Email")).sendKeys(string);

	}

	@When("user should enter the valid {string} in text box")
	public void user_should_enter_the_valid_in_text_box(String string) {
		driver.findElement(By.id("Password")).sendKeys(string);

	}

	@When("click on login button")
	public void click_on_login_button() {
		driver.findElement(By.xpath("(//input[@type='submit'])[2]")).click();

	}

	@Then("user be at home page gets name displayed")
	public void user_be_at_home_page_gets_name_displayed() {
		System.out.println("the homepage with logged user name displayed");

		
	}

	@Then("click on logout option")
	public void click_on_logout_option() {
		driver.findElement(By.linkText("Log out")).click();

	}

	@Then("user should be at homepage of demowebshop")
	public void user_should_be_at_homepage_of_demowebshop() {
		System.out.println("user should taken to the home page");

	}

	@Then("The test is Ended close the browser")
	public void the_test_is_ended_close_the_browser() {
		driver.close();

	}


}
